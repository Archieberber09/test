// Javascript template
preload()
var images,
    loadedImage = 0;

function preload() {
    images = new Array(
        // list images needed from the assets folder
        images = 'images/background.jpg',
        images = 'images/female.png',
        images = 'images/headline1.png',
        images = 'images/headline2.png',
        images = 'images/subheadline.png',
        images = 'images/button.png',
        images = 'images/logo.png',
        images = 'images/replay.png'
     
        
        
    );
   
    for (var i = 0; i < images.length; i++) {
        imageObj = new Image();
        imageObj.src = images[i];
        imageObj.addEventListener("load", iLoad, false)
    }
}



function iLoad() {
    loadedImage++;
    if (images.length == loadedImage) {
        
        // set the background for each div by calling the images declared on the array
        if( images.length == loadedImage) {
            main.style.backgroundImage = 'url("' + images [0] + '")'; 

            var female = document.createElement("IMG");
            female.setAttribute('class','female')
            female.setAttribute("src", images[1]);
            female.setAttribute("alt", "female");
            bgImage.appendChild(female);

            var headline1 = document.createElement("IMG");
            headline1.style.transform = 'translate(-150px,0)'
            headline1.setAttribute('class','headline1')
            headline1.setAttribute("src", images[2]);
            headline1.setAttribute("alt", "headline1");
            bgImage.appendChild(headline1);

            var headline2 = document.createElement("IMG");
            headline2.style.transform = 'translate(-180px,0)'
            headline2.setAttribute('class','headline2')
            headline2.setAttribute("src", images[3]);
            headline2.setAttribute("alt", "headline2");
            bgImage.appendChild(headline2);

            var subheadline = document.createElement("IMG");
            subheadline.style.opacity = '0%'
            subheadline.setAttribute('class','subheadline')
            subheadline.setAttribute("src", images[4]);
            subheadline.setAttribute("alt", "subheadline");
            bgImage.appendChild(subheadline);

            var button = document.createElement("IMG");
            button.style.opacity = '0%'
            button.setAttribute('class','button')
            button.setAttribute("src", images[5]);
            button.setAttribute("alt", "button");
            bgImage.appendChild(button);

            var logo = document.createElement("IMG");
            logo.style.transform = 'translate(150px,0)'
            logo.setAttribute('class','logo')
            logo.setAttribute("src", images[6]);
            logo.setAttribute("alt", "logo");
            bgImage.appendChild(logo);

            var replay = document.createElement("IMG");
            replay.setAttribute('class','replay')
            replay.setAttribute("src", images[7]);
            replay.setAttribute("alt", "replay");
            bgImage.appendChild(replay);

            replay.addEventListener('click',function(){
                location.reload()
            })

            }
            
            init();
       
    }
}

function init() {
    main.style.visibility = "visible";
}



// image preload function should be called upon banner load.


